<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/index.css">
<script src="${pageContext.request.contextPath}/assets/js/index.js"></script>
<link href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/lib/js/jquery.min.js"></script>

<div class="container">
    <div class="card card-container">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>
        <p id="profile-name" class="profile-name-card"></p>
        <form action="${pageContext.request.contextPath}/sec-login" method="post">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" name="uname" class="form-control" placeholder="Email address" required autofocus>
            <input type="password" name="passwd" class="form-control" placeholder="Password" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form><!-- /form -->
        <a href="#" class="forgot-password">
            Forgot the password?
        </a>
    </div><!-- /card-container -->
</div><!-- /container -->
</body>
</html>