urlParamReader = function (name) {
    let result = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.href);

    if (result) {
        return decodeURI(result[1] || 0);
    }
    return null;
};

$(document).ready(() => {

    let productId = urlParamReader('id');
    if (productId) {

        $.get(_contextPath + '/api/product/' + productId, (data, status) => {

            console.log(status);
            if (status === 'success') {
                $('#product-name').val(data.name);
                $('#product-price').val(data.price);
                $('#stock-name').val(data.stock.name);
                $('#stock-balance').val(data.stock.balance);
                $('#stock-amount').val(data.stock.amount);

            } else {
                window.alert('Error server not responsing')
            }
        });
    }

    $('#edit-btn').click(() => {
        let object = {
            name: $('#product-name').val(),
            price: $('#product-price').val(),
            stock: {
                name: $('#stock-name').val(),
                balance: $('#stock-balance').val(),
                amount: $('#stock-amount').val()
            }
        };

        $.ajax(_contextPath + '/api/product/' + productId, {
            data: JSON.stringify(object),
            contentType: 'application/json',
            type: 'PUT'
        });
    });

    $('#submit-btn').click(() => {
        window.location.href = _contextPath + '/admin/product/list';
    });


});