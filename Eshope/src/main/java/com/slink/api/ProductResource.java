package com.slink.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.slink.api.dto.ProductDTO;
import com.slink.core.Resource;
import com.slink.model.product.Product;
import com.slink.model.product.service.ProductService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequestScope
@Path("/product")
public class ProductResource extends Resource {

    private final ProductService productService;


    @Autowired
    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

    @GET
    public List<ProductDTO> main() {
        return productService.findAll()
                .stream().map(ProductDTO::new)
                .collect(Collectors.toList());
    }

    @POST
    public Response newProduct(ProductDTO dto) {
        dto.getStock().setBalance(100);
        Product product = dto.toObject();
        productService.add(product);
        return Response.status(Response.Status.CREATED).build();
    }

    @GET
    @Path("/{id}")
    public ProductDTO product(@PathParam("id") Integer id) {
        return new ProductDTO(productService.getById(id));
    }

    @PUT
    @Path("/{id}")
    public Response product(@PathParam("id") Integer id, ProductDTO dto) {
        dto.setId(id);
        productService.update(dto.toObject());
        return Response.accepted().build();
    }

    @POST
    @Path("/buy")
    public Response buyProduct(BuyDTO dto) {
        try {
            productService.buy(dto.getId(), dto.count);
            return Response.accepted().build();

        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class BuyDTO implements Serializable {
        @JsonProperty("productId")
        private Integer id;
        @JsonProperty("productCount")
        private Integer count;
    }


}
