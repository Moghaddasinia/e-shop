package com.slink.api.dto;

import com.slink.model.stock.Stock;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class StockRegDto implements Serializable {
    private Integer id;
    private String name;
    private Integer balance;
    private Double amount;


    public Stock toObject() {// convert dto (UserRegisterDTO) to obj (User)

        return new Stock(id, name, balance, amount, null);
    }

    public StockRegDto toDTO(Stock stock)  {// convert obj (User) to dto (UserRegisterDTO)

        this.id = stock.getId();
        this.name = stock.getName();
        this.balance = stock.getBalance();
        this.amount = stock.getAmount();

        return this;
    }





}
