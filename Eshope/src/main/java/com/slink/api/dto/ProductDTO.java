package com.slink.api.dto;

import com.slink.model.product.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ProductDTO implements Serializable {

    private Integer id;
    private String name;
    private BigDecimal price;

    private StockRegDto stock;

    public ProductDTO(Product product) {
        toDTO(product);
    }

    public Product toObject() {
        Product product = new Product();
        product.setId(this.id);
        product.setName(this.name);
        product.setPrice(this.price);
        product.setStock(this.stock.toObject());

        return product;
    }

    public ProductDTO toDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();

        StockRegDto stockRegDto = new StockRegDto();
        this.stock = stockRegDto.toDTO(product.getStock());

        return this;
    }
}
