package com.slink.model.stock.service;

import com.slink.model.stock.Stock;
import com.slink.model.stock.dao.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockService {

    @Autowired
    StockRepository stockRepository;


    @Autowired
    public StockService(StockRepository stockRepository) {

        this.stockRepository = stockRepository;
    }


    public List<Stock> findAll() {

        return stockRepository.findAll();
    }

}
