package com.slink.model.stock;


import com.slink.model.product.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor //hibernate need
@AllArgsConstructor //dto use
@Table(name = "stock")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;
    protected String name;
    protected Integer balance;
    protected Double amount;

    @OneToOne(mappedBy = "stock")
    protected Product product;

}
