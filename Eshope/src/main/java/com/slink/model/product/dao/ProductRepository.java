package com.slink.model.product.dao;

import com.slink.model.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Modifying
    @Transactional
    @Query("update Product p set p.stock.balance = p.stock.balance - :countX where p.id = :id")
    void buyProduct(@Param("id") Integer id, @Param("countX") int countOfProduct);
}
