package com.slink.model.product.service;

import com.slink.model.product.Product;
import com.slink.model.product.dao.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;


    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public void add(Product product) {
        this.productRepository.save(product);
    }

    public Product getById(Integer id) {
        return this.productRepository.findById(id).orElse(null);
    }

    public void update(Product toObject) {
        productRepository.save(toObject);
    }

    public void buy(Integer id, Integer productCount) throws Exception {
        Optional<Product> byId = productRepository.findById(id);
        if (byId.isPresent()) {
            Product product = byId.get();
            int i = product.getStock().getBalance() - productCount;
            if (i >= 0) {
                product.getStock().setBalance(i);
                productRepository.save(product);
            } else {
                throw new RuntimeException("Product number is low");
            }
        } else {
            throw new Exception("Product not found");
        }
    }
}
