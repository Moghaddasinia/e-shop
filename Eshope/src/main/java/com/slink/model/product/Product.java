package com.slink.model.product;

import com.slink.model.stock.Stock;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor //hibernate need
@AllArgsConstructor //dto use
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;
    protected String name;
    protected BigDecimal price;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(nullable = false)
    protected Stock stock;
}
