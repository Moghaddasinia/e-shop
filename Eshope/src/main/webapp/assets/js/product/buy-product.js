urlParamReader = function (name) {
    let result = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.href);

    if (result) {
        return decodeURI(result[1] || 0);
    }
    return null;
};

$(document).ready(() => {

    let productId = urlParamReader('id');
    if (productId) {

        $.get(_contextPath + '/api/product/' + productId, (data, status) => {

            console.log(status);
            if (status === 'success') {
                $('#product-name').val(data.name);
                $('#product-price').val(data.price);
                $('#stock-name').val(data.stock.name);
                $('#stock-balance').val(data.stock.balance);
                $('#stock-amount').val(data.stock.amount);

            } else {
                window.alert('Error server not responsing')
            }
        });
    }




    $('#buy-btn').click(() => {
        let productCount = $('#stock-count').val();

        if (Number(productCount) <= 0 || Number(productCount) >= $('#stock-balance').val()) {
            window.alert('You cant buy this number');
            return;
        }
        let object = {
            productId: Number(productId),
            productCount: Number(productCount)
        };

        $.ajax(_contextPath + '/api/product/buy', {
            data: JSON.stringify(object),
            contentType: 'application/json',
            type: 'POST'
        });


        window.alert("Your purchase has been successfully completed")
        window.location.href = _contextPath + '/product/listbuy';

    });

    $('#cancel-btn').click(() => {
        window.location.href = '../../admin/product';
    });


});