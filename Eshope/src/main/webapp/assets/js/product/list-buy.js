$(document).ready(() => {

    $.get(_contextPath + '/api/product', (data, status) => {

        $.each(data, (index, value) => {

            $("#product-list > tbody").append(`<tr>
    <td>${value.name}</td>
    <td>${value.price}</td>
    <td>${value.stock.name}</td>
    <td>${value.stock.balance}</td>
    <td>
        <p data-placement="top" data-toggle="tooltip" title="Edit">
            <button class="btn btn-primary btn-xs" onclick="editProduct(${value.id})" data-title="Edit"><span
                    class="glyphicon glyphicon-pencil"></span></button>
        </p>
    </td>
   
</tr>`);
        });
        $('#product-list').DataTable();

    });
});

editProduct = function (productId) {
    window.location.href = _contextPath + '/product/buy?id=' + productId;
};