$(document).ready(() => {
    $('#submit-btn').click(() => {
        let object = {
            name: $('#product-name').val(),
            price: $('#product-price').val(),
            stock: {
                name: $('#stock-name').val(),
                balance: 100,
                amount: $('#stock-amount').val()
            }

        };

        $.ajax(_contextPath + '/api/product', {
            data: JSON.stringify(object),
            contentType: 'application/json',
            type: 'POST'
        });
        window.alert("product add to db")
        window.location.href = _contextPath + '/admin/product/list';
    });


});