<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <script src="${pageContext.request.contextPath}/lib/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var _contextPath = "${pageContext.request.contextPath}";
    </script>
    <script src="${pageContext.request.contextPath}/assets/js/admin/add-product.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h3>Add New Product</h3>
        </div>
    </div>

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <i class="icon-calendar"></i>
                    <h3 class="panel-title">Product</h3>
                </div>


                <div class="panel-body">
                    <div class="input-group">
                        <span class="input-group-addon">Name</span>
                        <input class="form-control" type="text" id="product-name" placeholder="Name">
                    </div>
                    <br/>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input class="form-control" type="number" id="product-price" placeholder="Price">
                    </div>
                    <br/>
                    <div class="input-group">
                        <span class="input-group-addon">S</span>
                        <input class="form-control" type="text" id="stock-name" placeholder="Stock Name">
                    </div>
                    <br/>
                    <div class="input-group">
                        <span class="input-group-addon">A</span>
                        <input class="form-control" type="number" id="stock-amount" placeholder="Stock Amount">
                    </div>
                    <br/>
                    <div class="input-group">
                        <span class="input-group-addon">B</span>
                        <input class="form-control" readonly="readonly" value="1000" type="number" id="stock-balance"
                               placeholder="Stock Balance">
                    </div>
                    <br/>
                    <button id="submit-btn" type="submit" class="btn btn-info" title="">Add New Product</button>
                    <br/>
                </div>
            </div>
        </div>
    </div>
    <!-- Row end -->

</div>

</body>
</html>