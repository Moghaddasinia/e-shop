<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="${pageContext.request.contextPath}/lib/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <script src="${pageContext.request.contextPath}/lib/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/js/jquery.dataTables.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        const _contextPath = "${pageContext.request.contextPath}";
    </script>
    <script src="${pageContext.request.contextPath}/assets/js/admin/list-product.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h3>List Product</h3>
        </div>
    </div>


    <div class="container">

        <div class="row">

            <div class="col-md-12">


                <table id="product-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Name product</th>
                        <th>Price Product</th>
                        <th>Name Stock</th>
                        <th>price Stock</th>
                        <th>Edit</th>

                    </tr>
                    </thead>

                    <tfoot>
                    <tr>
                        <th>Name product</th>
                        <th>Price Product</th>
                        <th>Name Stock</th>
                        <th>price Stock</th>
                        <th>Edit</th>

                    </tr>
                    </tfoot>

                    <tbody></tbody>
                </table>


            </div>
        </div>
    </div>

    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure
                        you want to delete this Record?
                    </div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ng-click="delete()" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok-sign"></span> Yes
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> No
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
</body>
</html>