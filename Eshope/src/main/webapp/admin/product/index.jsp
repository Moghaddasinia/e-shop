<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="${pageContext.request.contextPath}/lib/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <script src="${pageContext.request.contextPath}/lib/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/lib/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var _contextPath = "${pageContext.request.contextPath}";
    </script>
    <script src="${pageContext.request.contextPath}/assets/js/admin/add-product.js"></script>


</head>
<body background="${pageContext.request.contextPath}/assets/image/stocks-1.jpg">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">WebSiteName</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../../admin/product/list">List</a></li>
                        <li><a href="../../admin/product/add">Add</a></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Product <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../../product/listbuy">List</a></li>

                    </ul>
                </li>


            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
        </div>
    </div>
</nav>




</body>
</html>